import { Router, Params } from "@angular/router";
import {Injectable} from '@angular/core'

@Injectable()
export class RoutingServices {
    constructor(private route:Router){}
    navigatePageUrl(url:string){
        this.route.navigate([url]);
    }
    navigatePageRelative(url:String,relative:String){
        relative?this.route.navigate([url]):this.route.navigate(['']);
    }
    navigatePageParams(url:String,relative:String,params:Params){

    }
}