import { Component, OnInit } from '@angular/core';
import{Router} from '@angular/router';

@Component({
  selector: 'app-launch-home',
  templateUrl: './launch-home.component.html',
  styleUrls: ['./launch-home.component.scss']
})
export class LaunchHomeComponent implements OnInit {

  constructor(private router:Router) { }
loadNext(){
  this.router.navigate(['launchpad/profile']);
}
  ngOnInit() {
  }

}
