import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-profile-1',
  templateUrl: './profile-1.component.html',
  styleUrls: ['./profile-1.component.scss']
})
export class Profile1Component implements OnInit {

    phoneCliked=false;
    emailCliked=false;
    textCliked=false;
    allCliked=false;

  onOptionClick(event){
   var  buttonName=event.target.innerText;
  
   switch(buttonName){
     case 'Phone':
          this.phoneCliked=!this.phoneCliked;
          break;
      case 'Email':
      this.emailCliked=!this.emailCliked;
          break;
      case 'Text':
      this.textCliked=!this.textCliked;
          break;
      case 'All':
          this.allCliked=!this.allCliked;
          if(this.allCliked){
          this.phoneCliked=true;
          this.emailCliked=true;
          this.textCliked=true;
          }else{
          this.phoneCliked=false;
          this.emailCliked=false;
          this.textCliked=false;
          }
          break;
   }

    if(this.emailCliked && this.textCliked && this.phoneCliked)
      this.allCliked=true;
    else 
     this.allCliked=false;
  }
  
 constructor(private router:Router) { }
loadNext(){
  this.router.navigate(['launchpad/profile2']);
}
loadPrevious(){
  console.log("Previous buttoon is cliked");
}
  ngOnInit() {
  }

}
