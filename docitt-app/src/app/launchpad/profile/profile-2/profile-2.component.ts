import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-profile-2',
  templateUrl: './profile-2.component.html',
  styleUrls: ['./profile-2.component.scss']
})
export class Profile2Component implements OnInit {

  constructor(private router:Router) { }
loadNext(){
  this.router.navigate(['launchpad/profile3']);
}
  ngOnInit() {
  }

}
