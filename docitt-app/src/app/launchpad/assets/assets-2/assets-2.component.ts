import { Component, OnInit } from '@angular/core';
import{Http,Headers, RequestOptions, Response} from '@angular/http';
import{Router} from '@angular/router';

@Component({
  selector: 'app-assets-2',
  templateUrl: './assets-2.component.html',
  styleUrls: ['./assets-2.component.scss']
})
export class Assets2Component implements OnInit {
  private jsonBody:any[];
  user = '';
  pass='';
  isClick:boolean = false;
  isSuccess:boolean = false;
  private headers:Headers;
  constructor(private http:Http,private router:Router) { }
goBack(){
  this.router.navigate(['launchpad/assets']);
}
  ngOnInit() {
  }
  getApi(){
    let headers = new Headers({'Content-Type': 'application/json'});
    // headers.append('Access-Control-Allow-Origin', '*');
    let options = new RequestOptions({headers:headers});
this.jsonBody = [
  {
"CustomerId": "591e5cc5c3310c354c4220d9",
   "InvitationId": "591e5c7dc3310c19f017d62c",
   "InstitutionId": "5915455dc3310c29783dc5a6",
    "Secert": this.pass,
    "UserName": this.user
}]
  // {
    //     "Secert": "plaid_good",
    // "UserName": "plaid_test"
  //     CustomerId:"591beaf3c3310c354c422088",
  //     InvitationId:"591beaedc3310c19f017d608",
  //     InstitutionId:"5915455dc3310c29783dc5a3",
  //     Secert: "plaid_good",
  //     UserName: "plaid_test"
  // }]
return this.http.post('https://sandbox.docitt.net/api/customer/591e5cc5c3310c354c4220d9/institution/5915455dc3310c29783dc5a6',this.jsonBody[0],options);
}
hitApi(){
  this.isClick = true;
  console.log(this.isClick);
  this.getApi().subscribe(
    (response)=>{this.isSuccess=true;console.log("success-flag:"+this.isSuccess+",click flag:"+this.isClick)},
    (error)=>{this.isSuccess=false;console.log("fail-flag"+this.isSuccess+",click flag:"+this.isClick)}
  );
  console.log(this.user,this.pass);
 }
}
