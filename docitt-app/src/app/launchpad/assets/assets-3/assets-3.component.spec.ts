import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Assets3Component } from './assets-3.component';

describe('Assets3Component', () => {
  let component: Assets3Component;
  let fixture: ComponentFixture<Assets3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Assets3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Assets3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
