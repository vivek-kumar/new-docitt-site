import { DocittAppPage } from './app.po';

describe('docitt-app App', () => {
  let page: DocittAppPage;

  beforeEach(() => {
    page = new DocittAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
